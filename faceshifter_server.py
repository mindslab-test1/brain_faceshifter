import cv2
import numpy as np
import torch
import argparse
import logging
import grpc
import time
from concurrent import futures

import faceshifter_pb2
from faceshifter_pb2 import OutputImg
from faceshifter_pb2_grpc import FaceShifterServicer, add_FaceShifterServicer_to_server

from faceshifter import FaceShifter

class FaceShifterServer(FaceShifterServicer):
    def __init__(self, model_path=["Arcface.pth", "", ""], use_HEARNet=False):
        model = FaceShifter(model_path, use_HEARNet)
        model.eval()
        self.model = model.cuda()

    def check_image_size(self, img):
        _, _, height, width = img.shape
        logging.debug('request width:%s, height:%s', width, height)
        if not (height == 256 and width == 256):
            return False
        else:
            return True

    def tensor_from_byte(self, image_bytes, context):
        img = cv2.imdecode(np.fromstring(image_bytes, dtype=np.uint8), cv2.IMREAD_COLOR)
        img = img * 1.0 / 255
        img = torch.from_numpy(np.transpose(img[:, :, [2, 1, 0]], (2, 0, 1))).float()
        img = img.unsqueeze(0)
        return img

    def do_faceshifter(self, target_image_bytes, source_image_bytes, context):
        target_img = self.tensor_from_byte(target_image_bytes, context)
        source_img = self.tensor_from_byte(source_image_bytes, context)

        if not (self.check_image_size(target_img) and self.check_image_size(source_img)):
            msg = 'Requested image size should be 256 x 256'
            logging.error(msg)
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details(msg)
            return bytes()

        target_img = target_img.cuda()
        source_img = source_img.cuda()

        with torch.no_grad():
            output = self.model.single_eval(target_img, source_img).data.squeeze().float().cpu().clamp_(0, 1).numpy()

        output = np.transpose(output[[2, 1, 0], :, :], (1, 2, 0))
        output = (output * 255.0).round()

        retval, img_buf = cv2.imencode('.png', output)
        if retval:
            output = img_buf.tobytes()
        else:
            output = bytes()
        return output


    def pre_generator(self, generator):
        for output in generator:
            output = output.data.squeeze().float().cpu().clamp_(0, 1).numpy()
            output = np.transpose(output[[2, 1, 0], :, :], (1, 2, 0))
            output = (output * 255.0).round()

            retval, img_buf = cv2.imencode('.png', output)
            if retval:
                output = img_buf.tobytes()
            else:
                output = bytes()
            yield output

    def do_faceinterpolation(self, target_image_bytes, source_image_bytes1, source_image_bytes2, context):
        target_img = self.tensor_from_byte(target_image_bytes, context)
        source_img1 = self.tensor_from_byte(source_image_bytes1, context)
        source_img2 = self.tensor_from_byte(source_image_bytes2, context)

        if not(self.check_image_size(target_img) and self.check_image_size(source_img1) and self.check_image_size(source_img2)):
            msg = 'Requested image size should be 256 x 256'
            logging.error(msg)
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details(msg)
            return bytes()

        target_img = target_img.cuda()
        source_img1 = source_img1.cuda()
        source_img2 = source_img2.cuda()

        interpolate_generator = self.model.get_interpolation_generator(target_img, source_img1, source_img2)
        generator = self.pre_generator(interpolate_generator)
        return generator

    def FaceSwap(self, request, context):
        try:
            req_in_image_bytes1 = request.imgBytes1
            req_in_image_bytes2 = request.imgBytes2
            out_img = self.do_faceshifter(req_in_image_bytes1, req_in_image_bytes2, context)
            response = OutputImg()
            response.imgBytes = out_img
            return response
        except Exception as e:
            logging.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))

    def FaceInterpolation(self, request, context):
        try:
            req_in_image_bytes1 = request.imgBytes1
            req_in_image_bytes2 = request.imgBytes2
            req_in_image_bytes3 = request.imgBytes3

            generator = self.do_faceinterpolation(req_in_image_bytes1, req_in_image_bytes2, req_in_image_bytes3, context)
            for output in generator:
                response = OutputImg()
                response.imgBytes = output
                yield response

        except Exception as e:
            logging.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='ESR runner executor')
    parser.add_argument('--model_arcface',
                        default='Arcface.pth',
                        help='Arcface Model Path.',
                        required=True,
                        type=str)
    parser.add_argument('--model_aei',
                        default='AEI_Net.pth',
                        help='AEI Net Model Path.',
                        required=True,
                        type=str)
    parser.add_argument('--model_hear',
                        default=None,
                        help='HEAR Net Model Path.',
                        type=str)
    parser.add_argument('-p', '--port',
                        nargs='?',
                        dest='port',
                        help='grpc port',
                        type=int,
                        default=57000)
    parser.add_argument('-l', '--log_level',
                        nargs='?',
                        dest='log_level',
                        help='logger level',
                        type=str,
                        default='INFO')
    args = parser.parse_args()

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1), )
    esr = FaceShifterServer([args.model_arcface, args.model_aei, args.model_hear], use_HEARNet=(False if args.model_hear == None else True))
    add_FaceShifterServicer_to_server(esr, server)
    server.add_insecure_port("[::]:{}".format(args.port))
    server.start()
    logging.basicConfig(
        level=getattr(logging, args.log_level),
        format='[%(levelname)s|%(filename)s:%(lineno)s][%(asctime)s] >>> %(message)s'
    )
    logging.info('FaceShifter starting at 0.0.0.0:%d', args.port)

    try:
        while True:
            time.sleep(60 * 60 * 24)

    except KeyboardInterrupt:
        server.stop(0)