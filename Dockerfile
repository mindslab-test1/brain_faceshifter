FROM nvcr.io/nvidia/pytorch:19.10-py3

WORKDIR /workspace
RUN cd /workspace
# install requirements
COPY . .
RUN pip install -r requirements.txt
RUN python -m grpc_tools.protoc -I . --python_out=. --grpc_python_out=. faceshifter.proto
RUN pip install --upgrade protobuf
#RUN python faceshifter_server.py