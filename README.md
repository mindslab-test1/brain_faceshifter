# FaceShifter_package

## Download pretraind files
download from https://drive.google.com/drive/folders/17aAsVrCv_FlFgtbNl3bJ_7pAZEfbsYh8?usp=sharing
save at `checkpoint` folder

## Build
`
docker build -t faceshifter:<version> .
`

## Run
```shell script
docker run -itd --ipc host --gpus gpu_number
-p url:port -v checkpoint:/checkpoint 
--name name -t faceshifter:<version> 
```

## Attach
`docker attach name`

## Start server
`python faceshifter_server.py --model_arcface /checkpoint/Arcface.pth --model_aei /checkpoint/AEI_Net.pth`

## Author
브레인 최창호