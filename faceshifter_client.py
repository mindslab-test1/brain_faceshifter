import os
import grpc

import io
from PIL import Image, ImageSequence

from faceshifter_pb2 import DoubleInputImg, TrippleInputImg
from faceshifter_pb2_grpc import FaceShifterStub

if __name__ == "__main__":
    os.makedirs("./test_result", exist_ok=True)

    image = DoubleInputImg()
    image.imgBytes1 = open("./test_img/00000003.png", "rb").read()
    image.imgBytes2 = open("./test_img/00000001.png", "rb").read()

    with grpc.insecure_channel("127.0.0.1:57000") as channel:
        stub = FaceShifterStub(channel)

        with open("./test_result/single.png", "wb") as f:
            output = stub.FaceSwap(image).imgBytes
            print(output)
            f.write(output)




    image = TrippleInputImg()
    image.imgBytes1 = open("./test_img/00000003.png", "rb").read()
    image.imgBytes2 = open("./test_img/00000001.png", "rb").read()
    image.imgBytes3 = open("./test_img/00000004.png", "rb").read()

    with grpc.insecure_channel("127.0.0.1:57000") as channel:
        stub = FaceShifterStub(channel)
        output = stub.FaceInterpolation(image)
        print("got output")
        image_idx = 0

        frames = []

        for img in output:
            print(image_idx)
            img = img.imgBytes
            open(f"./test_result/inter_{image_idx}.png", "wb").write(img)
            image_idx +=1

            im = Image.open(io.BytesIO(img))
            frames.append(im)

        frames[0].save("./test_result/inter.gif",
                       save_all=True, append_images=frames[1:], optimize=False, duration=40, loop=0)
