import os
import argparse
from PIL import Image
from collections import OrderedDict

import torch
import torch.nn as nn
import torch.nn.functional as F

from torchvision import transforms
from torchvision.models.resnet import resnet101

from model.AEINet import ADDGenerator, MultilevelAttributesEncoder
from model.HEARNet import HEAR_Net


class FaceShifter(nn.Module):
    def __init__(self, model_path=["Arcface.pth", "", ""], use_HEARNet=False):
        super(FaceShifter, self).__init__()
        self.use_HEARNet = use_HEARNet

        self.G = ADDGenerator()
        self.E = MultilevelAttributesEncoder()
        self.Z = resnet101(num_classes=256)
        if use_HEARNet:
            self.HEAR_Net = HEAR_Net()
        self.load_model(model_path)

    def load_model(self, model_path):
        Arcface_checkpoint, AEI_Net_checkpoint, HEAR_Net_checkpoint = model_path
        checkpoint = torch.load(Arcface_checkpoint, map_location='cpu')
        self.Z.load_state_dict(checkpoint['Network'])

        checkpoint = torch.load(AEI_Net_checkpoint, map_location='cpu')
        new_Embedder = OrderedDict()
        for k, v in checkpoint['Embedder'].items():
            name = k[7:]  # remove `module.`
            new_Embedder[name] = v
        self.E.load_state_dict(new_Embedder)

        new_Generator = OrderedDict()
        for k, v in checkpoint['Generator'].items():
            name = k[7:]  # remove `module.`
            new_Generator[name] = v
        self.G.load_state_dict(new_Generator)

        if self.use_HEARNet:
            checkpoint = torch.load(HEAR_Net_checkpoint, map_location='cpu')
            self.HEAR_Net.load_state_dict(checkpoint['Network'])

    @torch.no_grad()
    def forward(self, target_img, z_id):
        feature_map = self.E(target_img)
        output_hat = self.G(z_id, feature_map)
        if self.use_HEARNet:
            output = self.HEAR_Net(torch.cat((output_hat, target_img - output_hat), dim=1))
        else:
            output = output_hat
        return output

    @torch.no_grad()
    def single_eval(self, target_img, source_img):
        z_id = F.normalize(self.Z(F.interpolate(source_img, size=112, mode='bilinear')))
        return self(target_img, z_id)

    @torch.no_grad()
    def get_interpolation_result(self, target_img, z_id1, z_id2, depth):
        z_id = F.normalize(z_id1 + z_id2)
        output = self(target_img, z_id)
        if depth <= 0:
            return [output]
        else:
            return self.get_interpolation_result(target_img, z_id1, z_id, depth-1) + [output] + \
                   self.get_interpolation_result(target_img, z_id, z_id2, depth-1)

    def get_interpolated_z_id(self, z_id1, z_id2, depth):
        z_id = F.normalize(z_id1 + z_id2)
        if depth <= 0:
            return [z_id]
        else:
            return self.get_interpolated_z_id(z_id1, z_id, depth - 1) + [z_id] + \
                   self.get_interpolated_z_id(z_id, z_id2, depth - 1)

    def get_interpolation_generator(self, target_img, source_img1, source_img2):
        z_id1 = F.normalize(self.Z(F.interpolate(source_img1, size=112, mode='bilinear')))
        z_id2 = F.normalize(self.Z(F.interpolate(source_img2, size=112, mode='bilinear')))
        z_id_arr = self.get_interpolated_z_id(z_id1, z_id2, 3)
        z_id_arr.insert(0, z_id1)
        z_id_arr.append(z_id2)

        generator = (self(target_img, z_id) for z_id in z_id_arr)
        return generator

    @torch.no_grad()
    def interpolation_eval(self, target_img, source_img1, source_img2):
        z_id1 = F.normalize(self.Z(F.interpolate(source_img1, size=112, mode='bilinear')))
        z_id2 = F.normalize(self.Z(F.interpolate(source_img2, size=112, mode='bilinear')))

        output1 = self(target_img, z_id1)
        output2 = self(target_img, z_id2)
        output_arr = self.get_interpolation_result(target_img, z_id1, z_id2, 3)
        output_arr.insert(0, output1)
        output_arr.append(output2)
        return output_arr